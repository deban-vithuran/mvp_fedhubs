import * as React from 'react'
import cs from 'classnames'

import * as config from 'lib/config'

import styles from './PageSocial.module.css'

interface SocialLink {
  name: string
  title: string
  icon: React.ReactNode
  href?: string
}

const socialLinks: SocialLink[] = [

  config.facebook && {
    name: 'facebook',
    href: `https://facebook.com/Fedhubs-104121975352874`,
    title: `Facebook @${config.facebook}`,
    icon: (
      <svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 24 24'>
        <path d='M0 12.067C0 18.033 4.333 22.994 10 24v-8.667H7V12h3V9.333c0-3 1.933-4.666 4.667-4.666c.866 0 1.8.133 2.666.266V8H15.8c-1.467 0-1.8.733-1.8 1.667V12h3.2l-.533 3.333H14V24c5.667-1.006 10-5.966 10-11.933C24 5.43 18.6 0 12 0S0 5.43 0 12.067Z' />
      </svg>
    )
  },

  config.instagram && {
    name: 'instagram',
    href: `https://instagram.com/${config.instagram}`,
    title: `Instagram @${config.instagram}`,
    icon: (
      <svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 1536 1536'>
        <path d='M1024 768q0-106-75-181t-181-75t-181 75t-75 181t75 181t181 75t181-75t75-181zm138 0q0 164-115 279t-279 115t-279-115t-115-279t115-279t279-115t279 115t115 279zm108-410q0 38-27 65t-65 27t-65-27t-27-65t27-65t65-27t65 27t27 65zM768 138q-7 0-76.5-.5t-105.5 0t-96.5 3t-103 10T315 169q-50 20-88 58t-58 88q-11 29-18.5 71.5t-10 103t-3 96.5t0 105.5t.5 76.5t-.5 76.5t0 105.5t3 96.5t10 103T169 1221q20 50 58 88t88 58q29 11 71.5 18.5t103 10t96.5 3t105.5 0t76.5-.5t76.5.5t105.5 0t96.5-3t103-10t71.5-18.5q50-20 88-58t58-88q11-29 18.5-71.5t10-103t3-96.5t0-105.5t-.5-76.5t.5-76.5t0-105.5t-3-96.5t-10-103T1367 315q-20-50-58-88t-88-58q-29-11-71.5-18.5t-103-10t-96.5-3t-105.5 0t-76.5.5zm768 630q0 229-5 317q-10 208-124 322t-322 124q-88 5-317 5t-317-5q-208-10-322-124T5 1085q-5-88-5-317t5-317q10-208 124-322T451 5q88-5 317-5t317 5q208 10 322 124t124 322q5 88 5 317z' />
      </svg>
    ),
  },


  config.linkedin && {
    name: 'linkedin',
    href: `https://www.linkedin.com/company/${config.linkedin}`,
    title: `LinkedIn @fedhubs`,
    icon: (
      <svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 24 24'>
        <path d='M6.5 21.5h-5v-13h5v13zM4 6.5C2.5 6.5 1.5 5.3 1.5 4s1-2.4 2.5-2.4c1.6 0 2.5 1 2.6 2.5 0 1.4-1 2.5-2.6 2.5zm11.5 6c-1 0-2 1-2 2v7h-5v-13h5V10s1.6-1.5 4-1.5c3 0 5 2.2 5 6.3v6.7h-5v-7c0-1-1-2-2-2z' />
      </svg>
    ),
  },


  config.feedback && {
    name: 'feedback',
    href: `https://roadmap.fedhubs.com/`,
    title: `Contribution à la roadmap Fedhubs`,
    icon: (
      <svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 1000 1000' >
        <path d='M497.9,693.4l63.7-63.7l-83.5-83.5l-63.7,63.7v30.8h52.8v52.7H497.9z M721.5,298.4L529.2,490.7c-6,6-6.6,12.1-0.5,18.1c6,6,12.1,5.5,18.1-0.6l192.3-192.3c6-6,6.6-12.1,0.6-18.1C733.6,291.8,727.5,292.3,721.5,298.4L721.5,298.4z M783.6,624.2v104.4c0,43.4-15.4,80.8-46.7,112.1c-30.8,30.8-68.1,46.2-111.5,46.2H168.2c-43.4,0-80.8-15.4-112.1-46.2C25.4,809.3,10,772,10,728.6V271.4c0-43.4,15.4-80.8,46.2-111.5c31.3-31.3,68.7-46.7,112.1-46.7h457.1c23.1,0,44.5,4.4,64.3,13.7c5.5,2.7,8.8,6.6,9.9,12.6c1.1,6-0.5,11.5-4.9,15.9l-26.9,26.9c-4.9,4.9-11,6.6-17.6,4.4c-8.2-2.2-16.5-3.3-24.7-3.3H168.2c-24.2,0-45,8.8-62.1,25.8c-17,17-25.8,37.9-25.8,62.1v457.1c0,24.2,8.8,45,25.8,62.1c17,17,37.9,25.8,62.1,25.8h457.1c24.2,0,45.1-8.8,62.1-25.8c17-17,25.8-37.9,25.8-62.1v-69.2c0-4.9,1.6-8.8,4.9-12.1l35.2-35.2C764.3,601.1,783.6,608.8,783.6,624.2L783.6,624.2z M730.8,218.7L889,376.9L519.9,746.1H361.6V587.9L730.8,218.7z M974.8,291.2l-50.5,50.5L766,183.5l50.5-50.5c20.3-20.3,54.4-20.3,74.7,0l83.5,83.5C995.1,236.8,995.1,270.9,974.8,291.2z'></path>
      </svg>
    )
  }


].filter(Boolean)

/*
  config.twitter && {
    name: 'twitter',
    href: `https://twitter.com/${config.twitter}`,
    title: `Twitter @${config.twitter}`,
    icon: (
      <svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 24 24'>
        <path d='M23.44 4.83c-.8.37-1.5.38-2.22.02.93-.56.98-.96 1.32-2.02-.88.52-1.86.9-2.9 1.1-.82-.88-2-1.43-3.3-1.43-2.5 0-4.55 2.04-4.55 4.54 0 .36.03.7.1 1.04-3.77-.2-7.12-2-9.36-4.75-.4.67-.6 1.45-.6 2.3 0 1.56.8 2.95 2 3.77-.74-.03-1.44-.23-2.05-.57v.06c0 2.2 1.56 4.03 3.64 4.44-.67.2-1.37.2-2.06.08.58 1.8 2.26 3.12 4.25 3.16C5.78 18.1 3.37 18.74 1 18.46c2 1.3 4.4 2.04 6.97 2.04 8.35 0 12.92-6.92 12.92-12.93 0-.2 0-.4-.02-.6.9-.63 1.96-1.22 2.56-2.14z' />
      </svg>
    )
  },
*/

export const PageSocial: React.FC = () => {
  return (
    <div className={styles.pageSocial}>
      {socialLinks.map((action) => (
        <a
          className={cs(styles.action, styles[action.name])}
          href={action.href}
          key={action.name}
          title={action.title}
          target='_blank'
          rel='noopener noreferrer'
        >
          <div className={styles.actionBg}>
            <div className={styles.actionBgPane} />
          </div>

          <div className={styles.actionBg}>{action.icon}</div>
        </a>
      ))}
    </div>
  )
}
