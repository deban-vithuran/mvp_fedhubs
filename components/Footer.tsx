import * as React from 'react'
import { FaInstagram } from '@react-icons/all-files/fa/FaInstagram'
import { FaFacebook } from '@react-icons/all-files/fa/FaFacebook'
import { FaEdit } from '@react-icons/all-files/fa/FaEdit'
import { FaLinkedin } from '@react-icons/all-files/fa/FaLinkedin'



import * as config from 'lib/config'

import styles from './styles.module.css'

// TODO: merge the data and icons from PageSocial with the social links in Footer

export const FooterImpl: React.FC = () => {


  return (
    <footer className={styles.footer}>
      <div className={styles.copyright}>Copyright 2022 {config.author}</div>


      <div className={styles.social}>

        {config.facebook && (
          <a
            className={styles.facebook}
            href={`https://facebook.com/fedhubs`}
            title={`Facebook @${config.facebook}`}
            target='_blank'
            rel='noopener noreferrer'
          >
            <FaFacebook />
          </a>
        )}

        {config.instagram && (
          <a
            className={styles.instagram}
            href={`https://instagram.com/${config.instagram}`}
            title={`Instagram @${config.instagram}`}
            target='_blank'
            rel='noopener noreferrer'
          >
            <FaInstagram />
          </a>
        )}

        {config.linkedin && (
          <a
            className={styles.linkedin}
            href={`https://www.linkedin.com/company/${config.linkedin}`}
            title={`LinkedIn @${config.linkedin}`}
            target='_blank'
            rel='noopener noreferrer'
          >
            <FaLinkedin />
          </a>
        )}

        {config.feedback && (
          <a
            className={styles.feedback}
            href={`https://roadmap.fedhubs.com/`}
            title={`Contribution à la roadmap Fedhubs`}
            target='_blank'
            rel='noopener noreferrer'
          >
            <FaEdit />
          </a>
        )}

        

      </div>
    </footer>

        /**   
         {config.twitter && (
          <a
            className={styles.twitter}
            href={`https://twitter.com/${config.twitter}`}
            title={`Twitter @${config.twitter}`}
            target='_blank'
            rel='noopener noreferrer'
          >
            <FaTwitter />
          </a>
        )}
        */ 
  )
}

export const Footer = React.memo(FooterImpl)
